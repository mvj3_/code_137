/**
 * MainActivity
 * 
 * @author zuolongsnail
 * 
 */
public class MainActivity extends Activity {
	private Button parseBtn;
	private ListView listView;
	private ArrayAdapter<Person> adapter;
	private ArrayList<Person> persons;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		parseBtn = (Button) findViewById(R.id.parse);
		listView = (ListView) findViewById(R.id.list);
		parseBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				persons = MainActivity.this.readXml();
				adapter = new ArrayAdapter<Person>(MainActivity.this,
						android.R.layout.simple_expandable_list_item_1, persons);
				listView.setAdapter(adapter);
			}
		});
	}

	private ArrayList<Person> readXml() {
		InputStream file = this.getClass().getClassLoader()
				.getResourceAsStream("persons.xml");
		// ①创建XML解析处理器
		SAXXmlContentHandler contentHandler = new SAXXmlContentHandler();
		try {
			// 创建一个SAXParserFactory
			SAXParserFactory factory = SAXParserFactory.newInstance();
			// ②创建SAX解析器
			SAXParser parser = factory.newSAXParser();
			// ③将XML解析处理器分配给解析器
			// ④对文档进行解析，将每个事件发送给处理器。
			parser.parse(file, contentHandler);
			file.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return contentHandler.getBooks();
	}
}